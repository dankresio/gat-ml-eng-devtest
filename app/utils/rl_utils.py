import logging

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

# Create logger
logger = logging.getLogger("dev")

# Set seaborn theme (just for figure aesthetics)
sns.set_theme()


def update_q_table(algorithm_type, curr_s, curr_a, r, next_s, next_a, q_value,
				   gamma, alpha):
	"""Update Q-value table according to obtained rewards.

	This function performs policy improvement step for current RL algorithm;
	that is, updates Q-value table according to environment responses to the
	actions taken by the agent.

	Args:
		algorithm_type (str): type of learning algorithm used to update
		 Q-value table.
		curr_s (int): current state of the environment.
		curr_a (int): current action taken by the agent.
		r (float): reward obtained from the environment at state curr_s when
		 the agent takes action curr_a.
		next_s (int): next state of the environment after having taken
		 action curr_a
		next_a (int): next action to be taken by the agent.
		q_value (np.array): current Q-value table.
		gamma (float): discount rate.
		alpha (float): learning rate or step-size parameter.

	Returns:
		- q_value (np.array): updated Q-value table
	"""
	# Compute TD error
	if algorithm_type == 'sarsa':
		delta = r +\
				gamma * q_value[next_s, next_a] - q_value[curr_s, curr_a]
	elif algorithm_type == 'q_learning':
		delta = r + \
				gamma * np.max(q_value[next_s, :]) - q_value[curr_s, curr_a]
	else:
		raise ValueError("Invalid algorithm_type: {}".format(algorithm_type))

	# Update Q-value table
	q_value[curr_s, curr_a] += alpha * delta

	return q_value


def select_a(env, curr_s, q_value, epsilon=0.1):
	"""Select an action from e-greedy policy.

	This function makes the agent to select an action from action space,
	according to an epsilon greedy policy: take a random action (exploration)
	with a probability of epsilon, and the action that maximizes long-term
	reward (exploitation) with a probability 1-epsilon.

	Args:
		env (gym.wrappers.time_limit.TimeLimit): OpenAI Gym's object containing
		 the environment of the problem to be solved by RL.
		curr_s (int): current state of the environment.
		q_value (np.array): Q-function tabular representation for each
		 existing state-action pair, at current time.
		epsilon (float): exploration parameters that determines the probability
		 with which the agent explores the environment (epsilon) or exploits
		 his knowledge (1-epsilon).

	Returns:
		- a (int): the action to be taken by the agent at current state curr_s.
	"""
	# sample a float value from a uniform distribution over [0, 1)
	if np.random.rand() < epsilon:
		# if the sampled float is less than the exploration probability, the
		# agent selects a random action (exploration)
		a = env.action_space.sample()
	else:
		# otherwise, the agent exploits his knowledge by means of a greedy
		# policy (exploitation)
		a = np.argmax(q_value[curr_s, :])
	return a


def moving_average(x, window_size):
	"""Moving average.

	This function computes the moving average of input signal x, in order to
	smooth the curve.

	Args:
		x (np.array): input sequence/signal over which to compute the moving
		 average.
		window_size (int): size of the averaging window.

	Returns:
		- mv_av (np.array): the moving average (or rolling mean) of input
		   sequence x.
	"""
	mv_av = np.convolve(x, np.ones(window_size) / window_size, mode='valid')

	return mv_av


def save_figure_report(history, window_size=100, fig_name="output_report"):
	"""Save a visual report for the execution of RL algorithm.

	This function saves a visual report which facilitates visual convergence
	analysis of the algorithm. The report consists of 4 figures:
		- Number of steps vs Episode: the number of steps needed for the agent
			to reach a terminal state at each episode, with the moving average
			of this number of steps overlaid.
		- Cumulative rewards vs Episode: the return (cumulative discounted
		 	reward) obtained at each episode.
		- Terminal rewards vs Episode: the reward obtained at the end of each
			episode, with the moving average of this terminal reward overlaid.
		- Epsilon vs Episode: the evolution of exploration probability epsilon
			along each episode.

	Args:
		history (np.array): list containing the cumulative reward of each
		 episode, the average cumulative rewards along all episodes, the reward
		 obtained at terminal state on each episode and the value of epsilon
		 at the end of each episode.
		window_size (int): size of the averaging window to be used to compute
		 moving average
		fig_name (str): filename to save the figure
	"""
	# Create figure where to plot results
	fig, ax = plt.subplots(2, 2, figsize=[12, 8])

	# Number of steps
	ax[0, 0].plot(history[:, 0], history[:, 1], '.')
	ax[0, 0].set_xlabel('Episode')
	ax[0, 0].set_ylabel('Number of steps')
	ax[0, 0].plot(history[window_size - 1:, 0], moving_average(history[:, 1],
															   window_size))

	# Cumulative reward
	ax[0, 1].plot(history[:, 0], history[:, 2], '.')
	ax[0, 1].set_xlabel('Episode')
	ax[0, 1].set_ylabel('Cumulative rewards')
	ax[0, 1].plot(history[:, 0], history[:, 4], '--')

	# Terminal reward
	ax[1, 0].plot(history[:, 0], history[:, 3], '.')
	ax[1, 0].set_xlabel('Episode')
	ax[1, 0].set_ylabel('Terminal rewards')
	ax[1, 0].plot(history[window_size - 1:, 0], moving_average(history[:, 3],
															   window_size))

	# Epsilon
	ax[1, 1].plot(history[:, 0], history[:, 5], '.')
	ax[1, 1].set_xlabel('Episode')
	ax[1, 1].set_ylabel('Epsilon')

	# Save figure
	fig.savefig("./reports/" + fig_name + ".png")
	logger.info("* Figure saved at reports/" + fig_name + ".png")
