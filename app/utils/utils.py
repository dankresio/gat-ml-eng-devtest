import argparse
import logging
import logging.config
import os

import yaml

DEFAULT_LEVEL = logging.INFO


def log_setup(log_cfg_path='logger.yaml'):
	"""Set up the logger.

	This function configures Python logger by means of a YAML configuration
	file located. The logger allows us to have a readable track of our app
	regarding runtime.

	Args:
		log_cfg_path (str): path to the YAML file containing configuration
		parameters of the logger.
	"""
	if os.path.exists(log_cfg_path):
		with open(log_cfg_path, 'rt') as cfg_file:
			try:
				config = yaml.safe_load(cfg_file.read())
				logging.config.dictConfig(config)
			except Exception as e:
				print('Error with file, using Default logging')
				logging.basicConfig(level=DEFAULT_LEVEL)
	else:
		logging.basicConfig(level=DEFAULT_LEVEL)
		print('Config file not found, using Default logging')


def parse_args():
	"""Parse input arguments.

	This function collect all input arguments which define our RL agent and
	environment.

	Returns:
		- args (argparse.Namespace): a Namespace object containing all input
		arguments to be used in our reinforcement learning task.
	"""
	parser = argparse.ArgumentParser(
		description='Use SARSA/Q-learning algorithm with '
					'epsilon-greedy policy.')
	parser.add_argument('-a', '--algorithm', default='q_learning',
						choices=['sarsa', 'q_learning'],
						help="Type of learning algorithm. "
							 "(Default: Q-learning)")
	parser.add_argument('-e', '--environment', default='FrozenLake8x8-v1',
						help="Name of the environment provided in the OpenAI "
							 "Gym. (Default: FrozenLake8x8-v1)")
	parser.add_argument('-n', '--nepisode', default='20000', type=int,
						help="Number of episodes. (Default: 20000)")
	parser.add_argument('-al', '--alpha', default='0.1', type=float,
						help="Learning rate (step-size parameter). "
							 "(Default: 0.1)")
	parser.add_argument('-ga', '--gamma', default='0.99', type=float,
						help="Discount rate. (Default: 0.99)")
	parser.add_argument('-ep', '--epsilon', default='0.8', type=float,
						help="Fraction of random exploration in the epsilon "
							 "greedy. (Default: 0.8)")
	parser.add_argument('-ed', '--epsilondecay', default='0.99', type=float,
						help="Decay rate of epsilon in the epsilon greedy. "
							 "(Default: 0.99)")
	parser.add_argument('-ms', '--maxstep', default='200', type=int,
						help="Maximum steps allowed in a episode. "
							 "(Default: 200)")
	parser.add_argument('-ka', '--kappa', default='0.01', type=float,
						help="Weight of the most recent cumulative reward for "
							 "computing its running average. (Default: 0.01)")

	args = parser.parse_args()

	return args
