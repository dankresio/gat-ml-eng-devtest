#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	test.py: Unit tests to test some simple functionalities of Q-learning/SARSA
	implementation.
"""

__author__ = "Daniel M. García-Ocaña Hernández"
__email__ = "danielman8@gmail.com"

import unittest

from src.app import main
from test.optimal_policies import *


class TestRlAlgorithm(unittest.TestCase):

	def test_algorithm_type(self):
		"""Test input parameter algorithm_type. """
		with self.assertRaises(AssertionError):
			main(algorithm_type="dqn", test=True)

	def test_full_pipe(self):
		"""Test the full implementation with a toy example.

		In this test, the full pipeline (algorithm implementation) is tested
		with a toy example that can be found at OpenAI Gym: FrozenLake-v1.
		To check if learned policy is correct, all 5 optimal policies are
		hand-coded and loaded, and the learned policy is compared against all
		of them.
		"""
		# Experimental setup
		env_type = "FrozenLake-v1"  # environment
		algorithm_type = "q_learning"  # agent
		n_episode = 10000
		max_step = 200

		policy = main(env_type, algorithm_type, n_episode, max_step, test=True)

		if (policy == optimal_policy_1).all():
			optimal_policy = optimal_policy_1
		elif (policy == optimal_policy_2).all():
			optimal_policy = optimal_policy_2
		elif (policy == optimal_policy_3).all():
			optimal_policy = optimal_policy_3
		elif (policy == optimal_policy_4).all():
			optimal_policy = optimal_policy_4
		else:
			optimal_policy = optimal_policy_5

		msg = "Policies must coincide for this simple problem"
		np.testing.assert_array_equal(policy, optimal_policy, err_msg=msg)


if __name__ == '__main__':
	unittest.main()
