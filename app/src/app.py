#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	app.py: Q-learning/SARSA implementation to be run in RL problems with an
	small and discrete space of states and actions.

	This implementation uses the average cumulative reward (ave_cumu_r) obtained
	at the end of each episode to control the exploration rate. In this way, we
	can deal with sparse reward environments such as FrozenLake8x8, ensuring
	enough exploration to reach convergence.
"""

__author__ = "Daniel M. García-Ocaña Hernández"
__email__ = "danielman8@gmail.com"

import logging

import gym
import numpy as np

from utils.rl_utils import save_figure_report, select_a, update_q_table
from utils.utils import log_setup, parse_args

# Random seed
np.random.seed(42)

# Set up logging configuration and create logger
log_setup("config/logger.yaml")
logger = logging.getLogger("dev")


def main(env_type="FrozenLake8x8-v1", algorithm_type="q_learning",
		 n_episode=20000, max_step=200, alpha=0.1, gamma=0.99, epsilon=0.8,
		 epsilon_decay=0.99, kappa=0.01, test=False):
	"""Run Reinforcement Learning task.


	Args:
		env_type (str): name of the environment provided in the OpenAI Gym.
		algorithm_type (str): type of learning algorithm ('sarsa' or
		 'q_learning')
		n_episode (int): number of episodes.
		max_step (int): maximum steps allowed in a episode.
		alpha (float): learning rate (step-size parameter).
		gamma (float): discount rate.
		epsilon (float): fraction of random exploration in the epsilon greedy
		 policy.
		epsilon_decay (float): decay rate of epsilon in the epsilon greedy.
		kappa (float): weight of the most recent cumulative reward for
		 computing its running average.
		test (bool): flag indicating if we are running the code as a unit
		 test (True) or as a regular execution (False)

	Returns:
		- greedy_action (np.array): the optimal policy learned after running
		   the chosen reinforcement learning algorithm (SARSA or Q-Learning).
	"""
	# If testing mode, disable logging
	if test:
		# disable all logging calls of severity CRITICAL (50) and below.
		logging.disable(level=50)  # CRITICAL = 50

	# Check that algorithm_type is one of the valid types (SARSA or Q-Learning)
	assert algorithm_type in ["sarsa", "q_learning"], \
		"Invalid algorithm_type: {}".format(algorithm_type)

	# Load the environment of our problem
	env = gym.envs.make(env_type)

	# Constraints imposed by the environment
	n_s = env.observation_space.n
	n_a = env.action_space.n

	logger.info("* Problem solve: {}".format(env_type))
	logger.debug("  - Num. states: {}".format(n_s))
	logger.debug("  - Num. actions: {}".format(n_a))
	logger.info("* Algorithm use: {}".format(algorithm_type))
	logger.debug("  - Num. episodes: {}".format(n_episode))
	logger.debug("  - Max. steps per episode: {}".format(max_step))

	# Initialization of Q-value table
	q_value = np.zeros([n_s, n_a])
	# Initialization of a list for storing simulation history
	history = []
	# Initialization of average cumulative discounted reward
	ave_cumu_r = None

	for i_episode in range(n_episode):
		# Reset the return (cumulative discounted reward) for this episode
		cumu_r = 0

		# Start a new episode and sample the initial state
		curr_s = env.reset()

		# Select the first action in this episode
		curr_a = select_a(env, curr_s, q_value, epsilon=epsilon)

		for i_step in range(max_step):
			# Get a result of your action from the environment
			next_s, r, done, _ = env.step(curr_a)

			# Update the cumulative discounted reward
			cumu_r = r + gamma * cumu_r

			# Select next action
			next_a = select_a(env, next_s, q_value, epsilon=epsilon)

			# Update Q-table for current state and selected action
			q_value = update_q_table(algorithm_type,
									 curr_s, curr_a,
									 r, next_s, next_a,
									 q_value, gamma, alpha)

			# Update current state and action
			curr_s, curr_a = next_s, next_a

			if done:
				# To deal with sparse reward environments, average cumulative
				# discounted reward is used to control the exploration rate
				# epsilon
				if ave_cumu_r is None:
					ave_cumu_r = cumu_r
				else:
					# compute online moving average
					ave_cumu_r = kappa * cumu_r + (1 - kappa) * ave_cumu_r

				if cumu_r > ave_cumu_r:
					# If current episode's return is greater than average
					# return from all previous episodes, that means that we
					# are converging. Thus, we'll bias the current policy
					# toward exploitation (epsilon is decayed exponentially)
					epsilon = epsilon * epsilon_decay

				# Save and log agent-environment status at the end of each
				# episode
				history.append([i_episode, i_step, cumu_r, r, ave_cumu_r, epsilon])
				logger.info("Episode: {0}\t Steps: {1:>4}\tCumuR: {2:>5.2f}\t"
					  "TermR: {3}\tAveCumuR: {4:.3f}\tEpsilon: {5:.3f}".format(
					   i_episode, i_step, cumu_r, r, ave_cumu_r, epsilon))

				break

	# Convert history to a numpy array
	history = np.array(history)

	# Create execution report (figure with algorithm evolution)
	if not test:
		fig_name = 'results-{}-{}'.format(env_type, algorithm_type)
		save_figure_report(history, fig_name=fig_name)

	# Q-value table
	np.set_printoptions(precision=3, suppress=True)
	logger.debug("Q-value table:\n{}".format(q_value))

	# Greedy policy
	greedy_action = np.zeros([n_s, n_a])
	greedy_action[np.arange(n_s), np.argmax(q_value, axis=1)] = 1
	logger.info("Greedy policy:\n{}".format(greedy_action))

	return greedy_action


if __name__ == "__main__":
	args = parse_args()

	# Experimental setup
	env_type = args.environment  # environment
	algorithm_type = args.algorithm  # agent
	n_episode = args.nepisode
	max_step = args.maxstep

	# Meta parameters for the RL agent
	alpha = args.alpha
	gamma = args.gamma
	epsilon = args.epsilon
	epsilon_decay = args.epsilondecay

	# Moving average parameters
	kappa = args.kappa

	# Run the agent in chosen environment, and get the optimal policy
	policy = main(env_type, algorithm_type, n_episode, max_step, alpha, gamma,
				  epsilon, epsilon_decay, kappa)
