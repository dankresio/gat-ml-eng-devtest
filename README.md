# ML engineer test
This readme file shows which environment I chose, the approach used to learn the optimal policy and how to run the code from CLI (Command Line Interface).

## The solution
In order to provide a working solution and show my coding skills, I decided to solve "FrozenLake8x8-v1" toy text environment, from OpenAI Gym.

As this environment has a finite, discrete and small states-and-actions spaces (64 states and 4 possible actions at each state), the approach I chose to solve the task was using simple tabular and well known algorithms like SARSA or Q-learning. Although my solution provides the implementation for both algorithms, all my experiments were run with Q-learning as it is an off-policy control technic that rapidly learns the optimal policy.

As state and action spaces are still small, we do not suffer of curse of dimensionality yet and so, more complex technics that make use of non-linear approximations of Bellman equations were not taken into account.

## Run the code
In order to be able to run the code on any computer, Dockerfile and requirements.txt files are provided to mount a simple Ubuntu 18.04 image with all required libraries available.

In this way, to run proposed solution we just have to move inside the folder where Dockerfile is located (in this case, app/) and type the following lines at command line:
```sh
docker build -t gat/ml_engineer_test:v1 .
docker run --rm -ti --name gat-test -v $(pwd)/reports:/gat_ml_engineer_test/reports -v $(pwd)/logs:/gat_ml_engineer_test/logs gat/ml_engineer_test:v1
```
Once the docker image is built and running, we will be inside container's shell and we'll have to type the following to run the solution:
```sh
python src/app.py
```
Default is using Q-learning algorithm to learn the optimal policy. If interested on running SARSA implementation, it can be done by typing the following:
```sh
python src/app.py -a sarsa
```
After the algorithm is run, a figure with a summary of the learning process is saved under reports/ folder, and the log of execution is saved under logs/ folder.

### app.py usage
If interested on running any of these two algorithms modifying any other parameters, app.py script can be called with the following parameters:
```tex
usage: app.py [-h] [-a {sarsa,q_learning}] [-e ENVIRONMENT] [-n NEPISODE]
              [-al ALPHA] [-ga GAMMA] [-ep EPSILON] [-ed EPSILONDECAY]
              [-ms MAXSTEP] [-ka KAPPA]

Use SARSA/Q-learning algorithm with epsilon-greedy policy.

optional arguments:
  -h, --help            show this help message and exit
  -a {sarsa,q_learning}, --algorithm {sarsa,q_learning}
                        Type of learning algorithm. (Default: Q-learning)
  -e ENVIRONMENT, --environment ENVIRONMENT
                        Name of the environment provided in the OpenAI Gym.
                        (Default: FrozenLake8x8-v1)
  -n NEPISODE, --nepisode NEPISODE
                        Number of episodes. (Default: 20000)
  -al ALPHA, --alpha ALPHA
                        Learning rate (step-size parameter). (Default: 0.1)
  -ga GAMMA, --gamma GAMMA
                        Discount rate. (Default: 0.99)
  -ep EPSILON, --epsilon EPSILON
                        Fraction of random exploration in the epsilon greedy.
                        (Default: 0.8)
  -ed EPSILONDECAY, --epsilondecay EPSILONDECAY
                        Decay rate of epsilon in the epsilon greedy. (Default:
                        0.99)
  -ms MAXSTEP, --maxstep MAXSTEP
                        Maximum steps allowed in a episode. (Default: 200)
  -ka KAPPA, --kappa KAPPA
                        Weight of the most recent cumulative reward for
                        computing its running average. (Default: 0.01)
```
You can get a helpful guide of all available parameters by code inspection, or typing the following:
```sh
python src/app.py -h
```
## Test app.py
In order to test our RL implementation, some tests were created by means of unittest library. These tests are located under test/ folder and can be run by typing the following command at container's shell:
```sh
python -m unittest
```
In this way, all tests located under test/ folder will be run. For the purpose of this ML engineer test, only two simple test were coded.

## Final comments
As this is just a test, the learned policy is not saved on disk. In case this was deployed to production, we would export the learned policy and all algorithm parameters to a .pickle file at a data/ folder, to then be consumed.
